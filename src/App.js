import React, { Component } from 'react';
import { connect } from 'react-redux'
import logo from './logo.svg';
import './App.css';

import * as counterActions from './actions/counterActions';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <p>{this.props.value}</p>
          <button onClick={this.props.increment}>increment</button>
          <button onClick={this.props.decrement}>decrement</button>
        </header>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  value: state.counter.value
})

const mapDispatchToProps =  (dispatch) => ({
  increment: () => dispatch(counterActions.increment()),
  decrement: () => dispatch(counterActions.decrement()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App)
