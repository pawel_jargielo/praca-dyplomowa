import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import error from './errorReducer';
import loading from './loadingReducer';
import counter from './counterReducer';


export default (history) => combineReducers({
  router: connectRouter(history),
  loading,
  error,
  counter
})

// export default combineReducers({
//   loading,
//   error
// });
