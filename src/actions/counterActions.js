import {
  INCREMENT,
  DECREMENT,
} from '../consts/counterActionTypes';

// dispatched when a user has been found in storage
export function increment() {
  return {
    type: INCREMENT,
  };
}

export function decrement() {
  return {
    type: DECREMENT
  };
}

